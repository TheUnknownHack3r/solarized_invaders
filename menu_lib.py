"""
menu_lib is a basic, yet versatile libary that 
provides basic gui elements for pygame-ce
"""

import pygame

version = "0.1.0"
scale = 1
print(f"menu_lib {version}")  # print info


# Functions
def set_scale(x):
    global scale
    scale = x


def get_scaled_mouse_pos():
    mouse_pos = pygame.mouse.get_pos()
    return (mouse_pos[0] * scale, mouse_pos[1] * scale)


# Classes
class Button(pygame.sprite.Sprite):
    def __init__(
        self,
        pos: list,
        size: list,
        image: pygame.Surface = None,
        key=None,
        clickable: bool = False,
        mouse_button: int = None,
    ):
        super().__init__()
        self.pos = pos
        self.rect_size = size

        if key != None:
            self.key = chr(key)
            self.pygame_key = key
        else:
            self.key = key
            self.pygame_key = key

        self.clickable = clickable
        self.mouse_button = mouse_button
        self.triggered = False

        self.image = image
        if self.image == None:
            self.rect = pygame.Rect(self.pos, self.rect_size)
        else:
            self.rect = self.image.get_rect(topleft=(self.pos))

    def get_triggered(self):
        keys = pygame.key.get_pressed()
        buttons = pygame.mouse.get_pressed()
        mouse_pos = get_scaled_mouse_pos()

        if self.key != None:
            if keys[self.pygame_key]:
                self.triggered = True

        if self.clickable:
            if buttons[self.mouse_button] and self.rect.collidepoint(mouse_pos):
                self.triggered = True
        return self.triggered
