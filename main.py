"""
Solarized Invaders, by TheUnknownHack3r.
This game uses the following licenses:
    Source Code: GNU General Public License v3.0
    Art, Music, & SFX: CC-BY-4.0
For more info about these licenses, see the
COPYING (for the software) and LICENSE (for the assets).
"""

# Import needed modules
import asyncio
import pygame
import menu_lib

# Initiate Pygame
pygame.init()
pygame.mixer.init()

# Constants
SKY_COLORS = [
    (147, 161, 161),
    (129, 147, 148),
    (111, 132, 135),
    (93, 117, 112),
    (74, 102, 108),
    (56, 88, 95),
    (37, 73, 81),
    (18, 58, 68),
    #    (0, 43, 54),  # probably should remove at some point
]
# fmt:off
BASE03  = "#002B36"
BASE02  = "#073642"
BASE01  = "#586E75"
BASE00  = "#657B83"
BASE0   = "#839496"
BASE1   = "#93A1A1"
BASE2   = "#EEE8D5"
BASE3   = "#FDF6E3"
YELLOW  = "#B58900"
ORANGE  = "#CB4B16"
RED     = "#DC322F"
MAGENTA = "#D33682"
VIOLET  = "#6C71C4"
BLUE    = "#268BD2"
CYAN    = "#2AA198"
GREEN   = "#859900"
# fmt:on

# Variables
scale_factor = 0.5
size = (1024, 512)
render_size = (size[0] * scale_factor, size[1] * scale_factor)

projectile_queue = []

menu_lib.set_scale(scale_factor)

# Timers


# Functions
def get_scaled_mouse_pos():
    mouse_pos = pygame.mouse.get_pos()
    return (mouse_pos[0] * scale_factor, mouse_pos[1] * scale_factor)


# Classes
class Player(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.max_speed = 10
        self.speed = 0.0
        self.acceleration = 0.1
        self.friction = 0.05

        self.last_shot = 0
        self.shot_delay = 700

        self.image = pygame.image.load("assets/graphics/player.png").convert_alpha()
        self.rect = self.image.get_frect(
            midbottom=(render_size[0] / 2, render_size[1] - 2)
        )
        self.mask = pygame.mask.from_surface(self.image)

        self.shoot_sound = pygame.mixer.Sound("assets/audio/shoot.wav")

    def handle_input(self, dt):
        global projectile_queue
        keys = pygame.key.get_pressed()
        current_time = pygame.time.get_ticks()

        if keys[pygame.K_SPACE] and current_time >= self.last_shot + self.shot_delay:
            pygame.mixer.Sound.play(self.shoot_sound)
            projectile_queue.append([0, self.rect.center])
            self.last_shot = pygame.time.get_ticks()
        if keys[pygame.K_a]:
            self.speed += -self.acceleration
            if self.speed <= -self.max_speed:
                self.speed = -self.max_speed
        if keys[pygame.K_d]:
            self.speed += self.acceleration
            if self.speed >= self.max_speed:
                self.speed = self.max_speed

    def movement(self, dt):
        self.speed = round(self.speed, 2)
        self.rect.x += self.speed * dt

        if self.rect.left < 0:
            self.rect.left = 0
            self.speed = -self.speed
        elif self.rect.right > render_size[0]:
            self.rect.right = render_size[0]
            self.speed = -self.speed

        if self.speed > 0:
            self.speed -= self.friction * dt
        elif self.speed < 0:
            self.speed += self.friction * dt

        if abs(self.speed) < self.friction:
            self.speed = 0

    def update(self, dt):
        self.handle_input(dt)
        self.movement(dt)


class Projectile(pygame.sprite.Sprite):
    def __init__(self, type, pos, owner):
        super().__init__()

        if type == 0:
            self.image = pygame.image.load(
                "assets/graphics/tiny_bullet.png"
            ).convert_alpha()
            if owner == 0:
                self.speed = -5
            elif owner == 1:
                self.speed = 5
            self.damage = 1

        self.rect = self.image.get_rect(center=pos)
        self.mask = pygame.mask.from_surface(self.image)

    def update(self, dt):
        self.rect.y += self.speed * dt
        if self.rect.y <= -16 or self.rect.y >= render_size[1] + 16:
            self.kill()


class Invader(pygame.sprite.Sprite):
    def __init__(self, gridPos: list, level: int, type: int = 0):
        super().__init__()

        if type == 0:
            self.image = pygame.image.load(
                "assets/graphics/invader.png"
            ).convert_alpha()
            self.image_size = 16
        elif type == 1:
            self.image = pygame.image.load(
                "assets/graphics/invader_strong.png"
            ).convert_alpha()
            self.image_size = 32

        self.spacing = 3
        self.speed = 0.125 * (level + 1)

        self.rect = self.image.get_frect(
            topleft=[
                gridPos[0] * self.image_size
                + gridPos[0]
                + self.spacing
                + (gridPos[0] * (self.spacing - 1)),
                gridPos[1] * self.image_size
                + gridPos[1]
                + self.spacing
                + (gridPos[1] * (self.spacing - 1)),
            ]
        )
        self.mask = pygame.mask.from_surface(self.image)

    def update(self, dt, dir):
        self.rect.x += (self.speed * dir) * dt


class Game:
    def __init__(self, init_window=True):  # Initialize instance
        if init_window:
            # Create the window
            self.size = size
            window_icon = pygame.image.load("assets/graphics/icon.png")
            pygame.display.set_icon(window_icon)
            self.display_surf = pygame.display.set_mode(self.size)
            pygame.display.set_caption("Solarized Invaders")

        # Set up rendering surface
        self.scale_size = render_size
        self.render_surf = pygame.Surface(self.scale_size)

        self.main_clock = pygame.time.Clock()
        self.logic_clock = pygame.time.Clock()
        self.render_surf = pygame.Surface(self.scale_size)
        self.font = pygame.font.Font("assets/fonts/slkscr.ttf", 17)

        # Set up logging of performance
        self.last_time_logic = 0
        self.last_time_render = 0
        self.framerate = 0.0
        self.frame_time = 0
        self.logic_time = 0
        self.render_time = 0

        # Setup framerate independence
        self.LOGIC_FPS = 60
        self.render_fps = 60
        self.dt = 1.0

        # Define constants

        # Define variables
        self.mouse_pos = (0, 0)

        self.max_level = len(SKY_COLORS) - 1
        self.level = 0
        self.level_active = False

        self.score = 0
        self.score_text = self.font.render(f"Score: {self.score}", False, BASE3)
        self.score_text_rect = self.score_text.get_rect(topleft=[1, 1])

        self.sky_color = SKY_COLORS[self.level]

        # Define images and sprites
        self.player = pygame.sprite.GroupSingle()
        self.player.add(Player())

        self.projectiles = pygame.sprite.Group()

        self.invaders = pygame.sprite.Group()
        self.invader_dir = 1
        self.invader_grid_size = [25, 4]

        self.invader_death_sound = pygame.mixer.Sound("assets/audio/invader_death.wav")

        self.logo = pygame.image.load("assets/graphics/logo2.png").convert_alpha()
        self.logo_rect = self.logo.get_rect(
            center=[render_size[0] / 2, render_size[1] / 2]
        )

        self.play_button = menu_lib.Button(
            [render_size[0] / 2 - 50, render_size[1] - 60],
            [100, 50],
            clickable=True,
            mouse_button=0,
        )

        # Define sounds

        # Game state
        self.game_state = 0
        self.running = True  # The game is now initilized

    def logic(self):  # Logic loop
        # Calculate dt & framerate
        self.dt = self.logic_clock.tick() / 1000 * self.LOGIC_FPS
        self.framerate = self.logic_clock.get_fps()

        self.last_time_logic = pygame.time.get_ticks()  # Start timer

        # Event loop
        self.mouse_pos = get_scaled_mouse_pos()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running = False

        if self.game_state == 1:
            # Logic
            self.player.update(self.dt)
            self.update_projectiles()

            if not self.level_active:
                self.spawn_invaders()
            self.update_invaders()

            for projectile in projectile_queue:
                if projectile[0] == 0:
                    self.projectiles.add(Projectile(projectile[0], projectile[1], 0))
            projectile_queue.clear()

        if self.play_button.get_triggered():
            self.game_state = 1
            self.play_button.triggered = False

        # Calculate logic_time
        self.logic_time = pygame.time.get_ticks() - self.last_time_logic

    def render(self):  # Render loop
        self.last_time_render = pygame.time.get_ticks()  # Start timer

        if self.game_state == 1:
            self.render_surf.fill(self.sky_color)  # Clear the screen

            # Rendering
            self.player.draw(self.render_surf)
            self.invaders.draw(self.render_surf)
            self.projectiles.draw(self.render_surf)

            self.render_surf.blit(self.score_text, self.score_text_rect)

        elif self.game_state == 0:
            self.render_surf.fill(BASE03)

            # Images
            self.render_surf.blit(self.logo, self.logo_rect)
            pygame.draw.rect(self.render_surf, ORANGE, self.play_button.rect)

            # Text
            title = self.font.render("Solarized Invaders", False, BASE0)
            title_rect = title.get_rect(midtop=[render_size[0] / 2, 4])
            self.render_surf.blit(title, title_rect)

            play_text = self.font.render("Play", False, BASE2)
            play_text_rect = play_text.get_rect(
                center=[
                    self.play_button.rect.center[0],
                    self.play_button.rect.center[1],
                ]
            )
            self.render_surf.blit(play_text, play_text_rect)

        # Scale up rendered surface
        pygame.transform.scale(self.render_surf, self.size, self.display_surf)

        # Calculate render_time
        self.render_time = pygame.time.get_ticks() - self.last_time_render

        # Update the display
        pygame.display.flip()
        self.main_clock.tick(self.render_fps)  # Limit render FPS

    def spawn_invaders(self):
        for y in range(self.invader_grid_size[1]):
            for x in range(self.invader_grid_size[0]):
                self.invaders.add(Invader([x, y], self.level))
        self.level_active = True

    def update_invaders(self):
        self.invaders.update(self.dt, self.invader_dir)
        for invader in self.invaders.sprites():
            if invader.rect.right >= render_size[0] - 3:
                self.invader_dir = -1
                for invader in self.invaders.sprites():
                    invader.rect.y += 1
            elif invader.rect.left <= 3:
                self.invader_dir = 1
                for invader in self.invaders.sprites():
                    invader.rect.y += 1

            if invader.rect.bottom >= render_size[1] - 17:
                self.game_state = 2

    def update_projectiles(self):
        self.projectiles.update(self.dt)
        for projectile in self.projectiles.sprites():
            for invader in self.invaders.sprites():
                if pygame.sprite.collide_mask(projectile, invader):
                    projectile.kill()
                    invader.kill()
                    pygame.mixer.Sound.play(self.invader_death_sound)

                    self.score += 1
                    self.score_text = self.font.render(
                        f"Score: {self.score}", False, BASE3
                    )
                    self.score_text_rect = self.score_text.get_rect(topleft=[1, 1])

    def check_level_status(self):
        if len(self.invaders) == 0:
            self.level_active = False
            self.level += 1
            self.sky_color = SKY_COLORS[level]


async def main():
    main_thread = Game()
    while main_thread.running:
        main_thread.logic()
        main_thread.render()

        await asyncio.sleep(0)  # very important, and keep it 0
        print(main_thread.framerate)

    # Quit the game
    pygame.mixer.quit()
    pygame.quit()


if __name__ == "__main__":
    asyncio.run(main())
