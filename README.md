Solarized Invaders, by TheUnknownHack3r.
This game uses the following licenses:
    Source Code: GNU General Public License v3.0
    Art, Music, & SFX: CC-BY-4.0
For more info about these licenses, see the
COPYING (for the software) and LICENSE (for the assets).